#pragma once

#include "unicorn/unicorn.h"

#include <stdint.h>
#include <unordered_map>
#include <vector>
#include <memory>

using rwfunc = bool (*)(uint64_t address, void* buffer, int size);

enum LRC_BITNESS {
	LRC_X86,
	LRC_X64,
};

struct LRC_context {
	const void* RAX = 0;
	const void* RBX = 0;
	const void* RCX = 0;
	const void* RDX = 0;
	const void* RSI = 0;
	const void* RDI = 0;
	const void* RBP = 0;
	const void* RSP = 0;
	const void* R8 = 0;
	const void* R9 = 0;
	const void* R10 = 0;
	const void* R11 = 0;
	const void* R12 = 0;
	const void* R13 = 0;
	const void* R14 = 0;
	const void* R15 = 0;
};

struct LRC {

	LRC(rwfunc read_, rwfunc write_, LRC_BITNESS bitness_);
	~LRC();
	uint64_t do_call(uint64_t remote_start_address_, LRC_context* context_);
	void writeback_and_flush(bool writeback, bool only_flush_write = false);


	uc_engine* uc;
	int bitness;
	bool no_cache_writes;
	
	std::unique_ptr<char[]> stack;
	void stack_reset();
	template<typename T> void stack_push(T val);
	
	struct emu_mem_map_record {
		void* hva_address;
		uc_hook hhandle;
		uint64_t flags;
	};

	std::unordered_map<uint64_t, emu_mem_map_record> code_page_mappings; //eVA page base <->  mapping record
	rwfunc read_fn, write_fn;
};


template<typename T>
void LRC::stack_push(T val) {

	uint64_t rsp, rsp_new;

	switch (bitness) {
	case LRC_X86:
		uc_reg_read(uc, UC_X86_REG_ESP, &rsp);
		rsp_new = rsp - sizeof(T);
		uc_mem_write(uc, rsp_new, &val, sizeof(T));
		uc_reg_write(uc, UC_X86_REG_ESP, &rsp_new);
		break;
	case LRC_X64:
		uc_reg_read(uc, UC_X86_REG_RSP, &rsp);
		rsp_new = rsp - sizeof(T);
		uc_mem_write(uc, rsp_new, &val, sizeof(T));
		uc_reg_write(uc, UC_X86_REG_RSP, &rsp_new);
		break;
	}
}